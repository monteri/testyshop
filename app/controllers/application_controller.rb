class ApplicationController < ActionController::Base
  acts_as_token_authentication_handler_for User, fallback_to_devise: false
  # respond_to :json
  protect_from_forgery with: :null_session

  private
  def authenticate_user_from_token!
    user_email = request.headers["HTTP_EMAIL"].presence
    user = user_email && User.where(email: user_email).first
    unless user && Devise.secure_compare(user.authentication_token, request.headers["HTTP_AUTHENTICATION_TOKEN"])
      render_unauthorized("Unauthorized user or incorrect authentication token")
    end
  end

  def authenticate_admin_from_token!
    user_email = request.headers["HTTP_EMAIL"].presence
    user = user_email && User.where(email: user_email).first
    unless user \
      && Devise.secure_compare(user.authentication_token, request.headers["HTTP_AUTHENTICATION_TOKEN"]) \
      && user.admin
      render_unauthorized("User is not admin or token credentials are invalid")
    end
  end

  def render_unauthorized(message)
    errors = { errors: [ { detail: message } ] }
    render json: errors, status: :unauthorized
  end
end
