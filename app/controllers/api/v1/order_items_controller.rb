class Api::V1::OrderItemsController < ApplicationController

  before_action :authenticate_user_from_token!

  def index
    if params[:user] and params.has_key?(:history)
      user = User.find(params[:user])
      p "user"
      p user
      order_items = OrderItem.where(user_id: params[:user])
      p order_items
      p "order items before"
      unless user.current_order.nil?
        order_items = order_items.where.not(order_id: user.current_order)
        "order items after"
        p order_items
      end
      render json: order_items, status: 200
    elsif params[:user]
      order_items = OrderItem.where(user_id: params[:user])
      render json: order_items, status: 200
    elsif params[:order]
      order_items = OrderItem.where(order_id: params[:order])
      render json: order_items, status: 200
    else
      order_items = OrderItem.all
      render json: order_items, status: 200
    end
  end

  def show
    order_item = OrderItem.find(params[:id])
    render json: order_item, status: 200
  end

  def create
    if params[:order_id]
      item_already_in_order = OrderItem.where(order_id: params[:order_id], product_id: params[:product_id]).first
      if item_already_in_order
        item_already_in_order.quantity += params[:quantity]
        if item_already_in_order.save
          render json: item_already_in_order, status: 201
        else
          render json: {errors: item_already_in_order.errors}, status: 422
        end
      else
        order_item = OrderItem.new(order_item_params)
        if order_item.save
          render json: order_item, status: 201
        else
          render json: {errors: order_item.errors}, status: 422
        end
      end
    else
      order_id = 0
      if OrderItem.all.maximum("order_id")
        order_id = OrderItem.all.maximum("order_id") + 1
      end
      params[:order_id] = order_id
      order_item = OrderItem.new(order_item_params)
      if order_item.save
        user = User.find(params[:user_id])
        user.current_order = order_item.order_id
        user.save
        render json: order_item, status: 201
      else
        render json: {errors: order_item.errors}, status: 422
      end
    end
  end

  def update
    order_item = OrderItem.find(params[:id])
    if order_item.update(order_item_params)
      render json: order_item, status: 200
    else
      render json: {errors: order_item.errors}, status: 422
    end
  end

  def destroy
    if params[:product]
      order_items = OrderItem.where(product_id: params[:product])
      order_items.destroy_all
      head 204
    else
      order_item = OrderItem.find(params[:id])
      order_item.destroy
      head 204
    end
  end

  private
  def order_item_params
    params.permit(:product_id, :order_id, :user_id, :quantity, :sold_price)
  end
end
