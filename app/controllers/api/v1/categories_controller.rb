class Api::V1::CategoriesController < ApplicationController

  before_action :authenticate_user_from_token!, only: [:index]
  before_action :authenticate_admin_from_token!, except: [:index]

  def index
    categories = Category.all
    render json: categories, status: 200

  end

  def show
    category = Category.find(params[:id])
    render json: category, status: 200
  end

  def create
    category = Category.new(category_params)
    if category.save
      render json: category, status: 201
    else
      render json: {errors: category.errors}, status: 422
    end
  end

  def update
    category = Category.find(params[:id])
    if category.update(category_params)
      render json: category, status: 200
    else
      render json: {errors: category.errors}, status: 422
    end
  end

  def destroy
    category = Category.find(params[:id])
    category.destroy
    head 204
  end

  private
  def category_params
    params.permit(:title)
  end
end
