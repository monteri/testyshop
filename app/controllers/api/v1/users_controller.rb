class Api::V1::UsersController < ApplicationController

  before_action :authenticate_admin_from_token!, except: [:index, :update]
  before_action :authenticate_user_from_token!, only: [:index, :update]

  def index
    if params[:by_email]
      users = User.where(email: params[:by_email]).first
      render json: users, status: 200
    else
      users = User.all
      render json: users, status: 200
    end

  end

  def show
    user = User.find(params[:id])
    render json: user, status: 200
  end

  def create
    user=User.new(user_params)
    user.confirmed_at = DateTime.now
    if user.save
      render json: user, status: 201
    else
      render json: {errors: user.errors}, status: 422
    end
  end

  def update
    user=User.find(params[:id])
    if params.has_key?(:current_order) and !user.current_order.nil?
      OrderItem.where(order_id: user.current_order).each do |order_item|
        order_item.sold_price = order_item.product.price * order_item.quantity
        order_item.save
      end
    end
    if user.update(user_params)
      render json: user, status:200
    else
      render json: {errors: user.errors},status: 422
    end

  end

  def destroy
    user=User.find(params[:id])
    user.destroy
    head 204
  end

  private
  def user_params
    params.permit(:email,:password,:password_confirmation, :admin, :current_order, :sold_price)
  end
end
