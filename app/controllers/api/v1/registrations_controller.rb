class Api::V1::RegistrationsController < ApplicationController
  before_action :ensure_params_exist, only: :create
  skip_before_action :verify_authenticity_token, :only => :create

  def create
    user = User.new user_params
    if user.save
      render json: {
          messages: "Sign Up Successfully",
          is_success: true,
          data: {user: user}
      }, status: :ok
    else
      render json: {
          messages: "Sign Up Failded",
          is_success: false,
          data: {}
      }, status: :unprocessable_entity
    end
  end

  private
  def user_params
    params.permit(:email, :password)
  end

  def ensure_params_exist
    return if params.present?
    render json: {
        messages: "Missing Params",
        is_success: false,
        data: {}
    }, status: :bad_request
  end
end
