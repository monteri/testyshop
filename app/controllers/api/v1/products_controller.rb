class Api::V1::ProductsController < ApplicationController

  before_action :authenticate_admin_from_token!, except: [:index]

  def index
    products = Product.all
    render json: products, status: 200
  end

  def show
    product = Product.find(params[:id])
    render json: product, status: 200
  end

  def create
    product = Product.new(product_params)
    if product.save
      render json: product, status: 201
    else
      render json: {errors: product.errors}, status: 422
    end
  end

  def update
    product = Product.find(params[:id])
    if product.update(product_params)
      render json: product, status: 200
    else
      render json: {errors: product.errors}, status: 422
    end
  end

  def destroy
    product = Product.find(params[:id])
    product.destroy
    head 204
  end

  private
  def product_params
    params.permit(:title, :image, :price, :category_id)
  end
end
