class Product < ApplicationRecord
  include Rails.application.routes.url_helpers

  has_many :order_items, dependent: :delete_all
  belongs_to :category
  has_one_attached :image
  validates :title, presence: true
  validates :price, presence: true
  validates :category, presence: true
  # validates :image, presence: true

  def image_url
    Rails.application.routes.url_helpers.rails_representation_url(
        image.variant(resize_to_limit: [200, 200]).processed, only_path: true
    )
  end
end
