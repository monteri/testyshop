import React, {Component} from 'react';
import util from "../../packs/util";
import {connect} from "react-redux";
import {changeInCart, checkOutCart, deleteFromCart, fetchCart} from "../../actions/cartAction";
import {showAlert} from "../../actions/alertAction";

class Cart extends Component {
  componentDidMount() {
    this.props.fetchCart()
  }
  state = {
    changeMode: null,
    quantity: 0
  }

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }

  handleSubmit = (e, item) => {
    e.preventDefault()
    if (this.state.quantity !== item.quantity) {
      if (this.state.quantity > 0) {
        this.props.changeInCart(this.props.cartItems, item, this.state.quantity)
        this.setState({
          changeMode: null,
          quantity: 0
        })
      } else {
        this.props.showAlert("Количество должно быть больше 0", "danger")
      }
    }
    this.setState({
      changeMode: null,
      quantity: 0
    })
  }

  render() {
    const cartBlock = {
      border: "1px solid #ccc",
      padding: "1em 16px",
      borderRadius: "16px"
    }
    const listItem = {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      width: "100%"
    }
    const horizontalListItem = {
      border: "none"
    }
    const {cartItems} = this.props
    return (
      <div style={cartBlock}>
        { cartItems.length === 0 ?
          "В корзине нету товаров" :
          (<div>Количество товаров в корзине: <strong>{cartItems.length}</strong></div>) }

        {cartItems.length > 0 &&
          (<div>
            <ul className="list-group my-2" style={listItem}>
              {cartItems.map(item =>
                <li className="list-group-item" style={listItem} key={item.id}>
                  <ul className="list-group list-group-horizontal">
                    <li className="list-group-item" style={horizontalListItem}>
                      <b>{item.product.title}</b>
                    </li>
                    { this.state.changeMode === item.id ? (
                      <li className="list-group-item" style={horizontalListItem}>
                        <form className="form-inline" onSubmit={(e) => this.handleSubmit(e, item)}>
                          <input
                            type="number"
                            className="form-control-sm"
                            id="quantity"
                            value={this.state.quantity}
                            onChange={this.handleChange}
                            placeholder="Количество"/>
                          <button type="submit" className="btn btn-success btn-sm ml-1">
                            <i className="fa fa-check" aria-hidden="true"></i>
                          </button>
                        </form>
                      </li>

                    ) : (
                    <li className="list-group-item" style={horizontalListItem}>
                      &nbsp;x{item.quantity}&nbsp;=&nbsp;{util.formatCurrency(item.product.price * item.quantity)}
                    </li>
                    ) }
                  </ul>
                  <span>
                    <button
                      className="btn btn-warning btn-sm mr-1"
                      onClick={(e) => {
                        if (this.state.changeMode === item.id){
                          this.setState({
                            changeMode: null,
                            quantity: 0
                          })
                        } else {
                          this.setState({
                            changeMode: item.id,
                            quantity: item.quantity
                          })
                        }
                      }}
                    >
                      <i className="fa fa-pencil" aria-hidden="true"></i>
                    </button>
                    <button
                      className="btn btn-danger btn-sm"
                      onClick={(e) => this.props.deleteFromCart(cartItems, item)}
                    >
                      <i className="fa fa-times" aria-hidden="true"></i>
                    </button>
                  </span>

                </li>
              )}
            </ul>
            <ul className="nav justify-content-end">
              <li className="nav-item">
                Всего: { util.formatCurrency(cartItems.reduce((acc, cur) => acc + cur.product.price * cur.quantity, 0)) }
              </li>
            </ul>
            <ul className="mt-2 nav justify-content-end">
              <li className="nav-item">
                <button
                  className="btn btn-success"
                  onClick={(e) => this.props.checkOutCart()}
                >
                  Оформить заказ
                </button>
              </li>
            </ul>
          </div>)
        }
      </div>
    );
  }
}

const mapStateToProps = state => ({
  cartItems: state.cartItems.items
})

const mapActionsToProps = {
  showAlert: showAlert,
  checkOutCart: checkOutCart,
  fetchCart: fetchCart,
  deleteFromCart: deleteFromCart,
  changeInCart: changeInCart
}

export default connect(mapStateToProps, mapActionsToProps)(Cart)