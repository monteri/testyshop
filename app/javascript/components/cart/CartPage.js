import React, {Component} from 'react';
import Cart from "./Cart";

class CartPage extends Component {

  render() {
    return (
      <div>
        <Cart />
      </div>
    );
  }
}

export default CartPage;