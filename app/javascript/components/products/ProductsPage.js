import React, { Component } from 'react'
import Products from "./Products";

class ProductsPage extends Component {

  render() {
    return (
      <div>
        <Products />
      </div>
    )
  }
}

export default ProductsPage