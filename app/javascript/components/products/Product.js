import React, {Component} from 'react';
import {connect} from "react-redux";
import {fetchProducts} from "../../actions/productsAction";
import {addToCart} from "../../actions/cartAction";
import util from "../../packs/util";

class Product extends Component {
  componentDidMount() {
    this.props.fetchProducts()
  }

  render() {
    const product = this.props.products.filter(p => p.id === parseInt(this.props.match.params.id))[0]
    return (
      <div className="row">

        <div className="col-md-6">
          <img
            className="img-fluid"
            src={product.image}
            alt={product.title}
          />
        </div>
        <div className="col-md-6 text-center">
          <h3>{product.title}</h3>
          <p>Цена: {util.formatCurrency(parseFloat(product.price))}</p>
          { Object.keys(this.props.user).length !== 0 && (
            <button
              className="btn btn-success"
              onClick={(e) => { this.props.addToCart(product) }}
            >
              Добавить в корзину
            </button>)
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.currentUser,
  products: state.products.items,
})

export default connect(mapStateToProps, {fetchProducts, addToCart})(Product)