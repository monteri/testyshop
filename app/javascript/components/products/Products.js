import React, {Component} from 'react'
import util from '../../packs/util'
import {connect} from 'react-redux'
import {fetchProducts} from "../../actions/productsAction";
import {Link} from "react-router-dom";
import {addToCart} from "../../actions/cartAction";

class Products extends Component {
  componentDidMount() {
    this.props.fetchProducts()
  }

  render() {
    const productItems = this.props.products.map(product => (
      <div className="col-md-4 mb-1" key={product.id}>
        <div className="card bg-light">
          <div className="card-header">
            <Link to={`products/${product.id}`}>
              <img
                src={product.image}
                alt={product.title}
                className="card-img-top"
              />
            </Link>
          </div>
          <div className="card-body">
            <Link
              to={`products/${product.id}`}
            >
              <h5 className="card-title">{product.title}</h5>
            </Link>
            <p className="card-text">
              {util.formatCurrency(parseFloat(product.price))}
            </p>
            { Object.keys(this.props.user).length !== 0 &&
              (<button
                style={{width: "100%"}}
                className="btn btn-primary"
                onClick={(e) => this.props.addToCart(product)}
              >
                Добавить в корзину
              </button>)
            }
          </div>
        </div>
      </div>
      )
    )
    return (
      <div className="row justify-content-between">
        {productItems}
      </div>

    )
  }
}

const mapStateToProps = state => ({
  user: state.user.currentUser,
  products: state.products.items,
  cartItems: state.cartItems.items
})

export default connect(mapStateToProps, {fetchProducts, addToCart})(Products)