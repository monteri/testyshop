import React, { Component } from 'react'
import {NavLink} from "react-router-dom";
import {connect} from "react-redux";
import {logoutUser} from "../../actions/userAction";

class Navbar extends Component {

  render() {
    const exitButton = {
      border: "none",
      background: "#f8f9fa"
    }

    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <NavLink
          className="navbar-brand"
          to="/"
        >
          TestShop
        </NavLink>
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <NavLink
              className="nav-link"
              to="/"
              exact
            >
              Каталог
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              className="nav-link"
              to="/cart">
              Корзина
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              className="nav-link"
              to="/history">
              История заказов
            </NavLink>
          </li>
          { this.props.user.admin && (
            <li className="nav-item">
              <NavLink
                className="nav-link"
                to="/admin"
              >
                Админ панель
              </NavLink>
            </li>
          )}
        </ul>
        { Object.keys(this.props.user).length === 0 ? (
          <ul className="navbar-nav">
            <li className="nav-item">
              <NavLink
                className="nav-link"
                to="/login"
              >
                Вход
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                className="nav-link"
                to="/sign-up"
              >
                Регистрация
              </NavLink>
            </li>
          </ul>
        ) : (
          <ul className="navbar-nav">
            <li className="nav-item">
              <span className="nav-link">
                { this.props.user.email }
              </span>
            </li>
            <li className="nav-item">
              <button
                style={exitButton}
                className="nav-link"
                onClick={() => this.props.logoutUser()}
              >
                Выход
              </button>
            </li>
          </ul>
        )}
      </nav>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.currentUser,
})

export default connect(mapStateToProps, {logoutUser})(Navbar)