import React, {Component} from 'react';
import {NavLink, Route, Redirect} from "react-router-dom";
import AdminUsers from "./admin_users/AdminUsers";
import AdminProducts from "./admin_products/AdminProducts";
import CreateAdminUser from "./admin_users/CreateAdminUser";
import ChangeAdminUser from "./admin_users/ChangeAdminUser";
import CreateAdminProduct from "./admin_products/CreateAdminProduct";
import ChangeAdminProduct from "./admin_products/ChangeAdminProduct";

class AdminPage extends Component {
  render() {
    const navLink = {
      color: "rgba(0,0,0, 0.7)"
    }
    return (
      <div>
        <ul className="list-group list-group-horizontal justify-content-center">
          <li className="list-group-item">
            <NavLink
              style={navLink}
              className="nav-link"
              to="/admin/users"
            >
              Пользователи
            </NavLink>
          </li>
          <li className="list-group-item">
            <NavLink
              style={navLink}
              className="nav-link"
              to="/admin/products"
            >
              Товары
            </NavLink>
          </li>
        </ul>
        <Route path="/admin/users" exact component={AdminUsers} />
        <Route path="/admin/users/create" exact component={CreateAdminUser} />
        <Route path="/admin/users/change/:id" exact component={ChangeAdminUser} />
        <Route path="/admin/products" exact component={AdminProducts} />
        <Route path="/admin/products/create" exact component={CreateAdminProduct} />
        <Route path="/admin/products/change/:id" exact component={ChangeAdminProduct} />
        <Redirect from="/admin" to="/admin/users" exact />
      </div>
    );
  }
}

export default AdminPage;