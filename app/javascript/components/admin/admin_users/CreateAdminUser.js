import React, {Component} from 'react';
import {connect} from "react-redux";
import {createUserAdmin} from "../../../actions/adminUsersAction";
import {withRouter} from "react-router-dom";
import {compose} from "redux";
import util from "../../../packs/util";
import {showAlert} from "../../../actions/alertAction";

class CreateAdminUser extends Component {

  state = {
    email: '',
    password: '',
    admin: false
  }

  handleSubmit = (e) => {
    e.preventDefault()
    if (this.state.email.length === 0){
      this.props.showAlert("Email не может быть пустым", "danger")
    } else if (this.state.password.length === 0) {
      this.props.showAlert("Пароль не может быть пустым", "danger")
    } else if (!util.validateEmail(this.state.email)) {
      this.props.showAlert("Формат email неправильный. Пример: example@email.com", "danger")
    } else if (this.state.password.length < 6) {
      this.props.showAlert("Пароль должен быть больше 6 символов", "danger")
    } else {
      this.props.createUserAdmin(this.state)
      this.props.history.push('/admin/users')
    }
  }

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.type === "checkbox" ? e.target.checked : e.target.value
    })
  }

  render() {
    return (
      <div>
        <div className="container">
          <h3>Создание пользователя</h3>
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label htmlFor="email">Email</label>
              <input
                onChange={this.handleChange}
                type="email"
                className="form-control"
                id="email"
                aria-describedby="emailHelp"
                placeholder="Введите email..."/>
            </div>
            <div className="form-group">
              <label htmlFor="password">Пароль</label>
              <input
                onChange={this.handleChange}
                type="password"
                className="form-control"
                id="password"
                placeholder="Введите пароль..."/>
            </div>
            <div className="form-group form-check">
              <input
                type="checkbox"
                className="form-check-input"
                id="admin"
                onChange={this.handleChange}
              />
              <label className="form-check-label" htmlFor="admin">Админ</label>
            </div>
            <button type="submit" className="btn btn-primary">Созадать</button>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  users: state.adminUsers.users,
})

export default compose(
  withRouter,
  connect(mapStateToProps, {showAlert, createUserAdmin})
)(CreateAdminUser)