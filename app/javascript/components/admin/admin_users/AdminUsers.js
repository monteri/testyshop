import React, {Component} from 'react';
import {connect} from "react-redux";
import {deleteUserAdmin, fetchUsersAdmin} from "../../../actions/adminUsersAction";
import {Link} from "react-router-dom";

class AdminUsers extends Component {
  componentDidMount() {
    this.props.fetchUsersAdmin()
  }

  render() {
    const userRecords = this.props.users.map(user => (
      <tbody key={user.id}>
        <tr className="text-center">
          <td style={{width: "5%"}}>{user.id}</td>
          <td style={{width: "60%"}}>{user.email}</td>
          { user.admin ?
            (<td style={{width: "20%"}}>true</td>) :
            (<td style={{width: "20%"}}>false</td>)
          }
          <td style={{width: "15%"}}>
            <Link to={`/admin/users/change/${user.id}`} className="btn btn-warning mr-1">
              <i className="fa fa-pencil" aria-hidden="true"></i>
            </Link>
            <button className="btn btn-danger" onClick={(e) => this.props.deleteUserAdmin(user)}>
              <i className="fa fa-times" aria-hidden="true"></i>
            </button>
          </td>
        </tr>
      </tbody>
      )
    )
    return (
      <div>
        <div>
          <h3>Пользователи</h3>
          <Link
            className="btn btn-success m-1 float-right"
            to="/admin/users/create"
          >
            Создать пользователя
          </Link>
        </div>
        <table className="table table-striped">
          <thead>
            <tr className="text-center">
              <th scope="col">ID</th>
              <th scope="col">Email</th>
              <th scope="col">Админ</th>
              <th scope="col">Управление</th>
            </tr>
          </thead>
          {userRecords}
        </table>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  users: state.adminUsers.users,
})

export default connect(mapStateToProps, {fetchUsersAdmin, deleteUserAdmin})(AdminUsers)