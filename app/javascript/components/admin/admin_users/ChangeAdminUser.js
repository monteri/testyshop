import React, {Component} from 'react';
import {connect} from "react-redux";
import {changeUserAdmin} from "../../../actions/adminUsersAction";
import {compose} from "redux";
import {withRouter} from "react-router-dom";
import util from "../../../packs/util";
import {showAlert} from "../../../actions/alertAction";

class ChangeAdminUser extends Component {
  constructor(props) {
    super(props);
    this.user = this.props.users.filter(u => u.id === parseInt(this.props.match.params.id))[0]
    console.log(this.user)
    this.state = {
      email: this.user.email,
      password: '',
      admin: this.user.admin
    }
  }

  handleSubmit = (e) => {
    e.preventDefault()
    if (this.state.email.length === 0){
      this.props.showAlert("Email не может быть пустым", "danger")
    } else if (this.state.password.length === 0) {
      this.props.showAlert("Пароль не может быть пустым", "danger")
    } else if (!util.validateEmail(this.state.email)) {
      this.props.showAlert("Формат email неправильный. Пример: example@email.com", "danger")
    } else if (this.state.password.length < 6) {
      this.props.showAlert("Пароль должен быть больше 6 символов", "danger")
    } else if (this.user.email === this.state.email &&
                this.user.password === '' &&
                this.user.admin === this.state.admin) {
      this.props.history.push('/admin/users')
    } else {
      this.user.email = this.state.email
      this.user.password = this.state.password
      this.user.admin = this.state.admin
      console.log(this.user)
      this.props.changeUserAdmin(this.user)
      this.props.history.push('/admin/users')
    }
  }

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.type === "checkbox" ? e.target.checked : e.target.value
    })
  }

  render() {
    return (
      <div>
        <div className="container">
          <h3>Изменение пользователя</h3>
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label htmlFor="email">Email</label>
              <input
                onChange={this.handleChange}
                type="email"
                className="form-control"
                id="email"
                aria-describedby="emailHelp"
                value={this.state.email || ''}
                placeholder="Введите email..."/>
            </div>
            <div className="form-group">
              <label htmlFor="password">Пароль</label>
              <input
                onChange={this.handleChange}
                type="password"
                className="form-control"
                id="password"
                placeholder="Введите пароль..."/>
            </div>
            <div className="form-group form-check">
              <input
                type="checkbox"
                className="form-check-input"
                id="admin"
                checked={this.state.admin || false}
                onChange={this.handleChange}
              />
              <label className="form-check-label" htmlFor="admin">Админ</label>
            </div>
            <button type="submit" className="btn btn-primary">Изменить</button>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  users: state.adminUsers.users,
})

export default compose(
  withRouter,
  connect(mapStateToProps, {showAlert, changeUserAdmin})
)(ChangeAdminUser)