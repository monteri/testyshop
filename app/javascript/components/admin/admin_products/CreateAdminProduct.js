import React, {Component} from 'react';
import {connect} from "react-redux";
import {compose} from "redux";
import {withRouter} from "react-router-dom";
import {createProductAdmin} from "../../../actions/adminProductsAction";
import {showAlert} from "../../../actions/alertAction";

class CreateAdminProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      price: '',
      image: null,
      imgSrc: null
    }
  }

  handleSubmit = (e) => {
    e.preventDefault()
    console.log(this.state)
    if (this.state.title.length === 0) {
      this.props.showAlert("Название не может быть пустым", "danger")
    } else if (this.state.price.length === 0) {
      this.props.showAlert("Цена не может быть пустой", "danger")
    } else if (isNaN(parseFloat(this.state.price))) {
      this.props.showAlert("Цена должна быть числом. Пример: 28 или 28.99", "danger")
    } else if (this.state.image === null) {
      this.props.showAlert("Фото не выбрано", "danger")
    } else {
      this.props.createProductAdmin(this.state)
      this.props.history.push('/admin/products')
    }
  }

  handleChange = (e) => {
    if (e.target.type === "file") {
      let reader = new FileReader();
      let url = reader.readAsDataURL(e.target.files[0]);

      reader.onloadend = function (e) {
        this.setState({
          imgSrc: [reader.result]
        })
      }.bind(this)
      console.log(url)
    }

    this.setState({
      [e.target.id]: e.target.type === "file" ? e.target.files[0] : e.target.value
    })
  }

  render() {
    return (
      <div>
        <div className="container">
          <h3>Создание товара</h3>
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label htmlFor="title">Название</label>
              <input
                onChange={this.handleChange}
                type="text"
                className="form-control"
                id="title"
                placeholder="Название"/>
            </div>
            <div className="form-group">
              <label htmlFor="price">Цена</label>
              <input
                onChange={this.handleChange}
                type="text"
                className="form-control"
                id="price"
                placeholder="Цена"/>
            </div>
            { this.state.image !== null &&
              (<img
                src={this.state.imgSrc}
                className="img-fluid"
                alt="img"
              />)
            }
            <div className="form-group">
              <input
                onChange={this.handleChange}
                type="file"
                className="form-control-file"
                id="image"/>
            </div>
            <button type="submit" className="btn btn-primary">Создать</button>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  products: state.adminProducts.products,
})

export default compose(
  withRouter,
  connect(mapStateToProps, {showAlert, createProductAdmin})
)(CreateAdminProduct)