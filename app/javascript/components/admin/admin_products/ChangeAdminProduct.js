import React, {Component} from 'react';
import {connect} from "react-redux";
import {compose} from "redux";
import {withRouter} from "react-router-dom";
import {changeProductAdmin} from "../../../actions/adminProductsAction";
import {showAlert} from "../../../actions/alertAction";

class ChangeAdminProduct extends Component {
  constructor(props) {
    super(props);
    this.product = this.props.products.filter(p => p.id === parseInt(this.props.match.params.id))[0]
    console.log(this.product)
    this.state = {
      title: this.product.title,
      price: this.product.price,
      image: null,
      imgSrc: null
    }
  }

  handleSubmit = (e) => {
    e.preventDefault()
    if (this.state.title.length === 0) {
      this.props.showAlert("Название не может быть пустым", "danger")
    } else if (this.state.price.length === 0) {
      this.props.showAlert("Цена не может быть пустой", "danger")
    } else if (isNaN(parseFloat(this.state.price))) {
      this.props.showAlert("Цена должна быть числом. Пример: 28 или 28.99", "danger")
    } else if (this.product.title === this.state.title &&
                this.product.price === this.state.price &&
                this.state.image === null) {
      this.props.history.push('/admin/products')
    } else {
      this.product.title = this.state.title
      this.product.price = this.state.price
      this.product.image = this.state.image
      console.log(this.product)
      this.props.changeProductAdmin(this.product)
      this.props.history.push('/admin/products')
    }
  }

  handleChange = (e) => {
    if (e.target.type === "file") {
      let reader = new FileReader();
      let url = reader.readAsDataURL(e.target.files[0]);

      reader.onloadend = function (e) {
        this.setState({
          imgSrc: [reader.result]
        })
      }.bind(this)
      console.log(url)
    }
    this.setState({
      [e.target.id]: e.target.type === "file" ? e.target.files[0] : e.target.value
    })
  }

  render() {
    return (
      <div>
        <div className="container">
          <h3>Изменение товара</h3>
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label htmlFor="title">Название</label>
              <input
                onChange={this.handleChange}
                type="text"
                className="form-control"
                id="title"
                value={this.state.title || ''}
                placeholder="Название"/>
            </div>
            <div className="form-group">
              <label htmlFor="price">Цена</label>
              <input
                onChange={this.handleChange}
                type="text"
                className="form-control"
                id="price"
                value={this.state.price || ''}
                placeholder="Цена"/>
            </div>
            { this.state.image === null ?
              (<img
                src={this.product.image}
                alt={this.product.title}
                className="img-fluid"
              />) :
              (<img
                src={this.state.imgSrc}
                alt={this.product.title}
                className="img-fluid"
              />)
            }
            <div className="form-group">
              <input
                onChange={this.handleChange}
                type="file"
                className="form-control-file"
                id="image"/>
            </div>
            <button type="submit" className="btn btn-primary">Изменить</button>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  products: state.adminProducts.products,
})

export default compose(
  withRouter,
  connect(mapStateToProps, {showAlert, changeProductAdmin})
)(ChangeAdminProduct)