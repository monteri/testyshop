import React, {Component} from 'react';
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import {deleteProductAdmin, fetchProductsAdmin} from "../../../actions/adminProductsAction";
import util from "../../../packs/util";

class AdminProducts extends Component {
  componentDidMount() {
    this.props.fetchProductsAdmin()
  }

  render() {
    const productRecords = this.props.products.map(product => (
        <tbody key={product.id}>
        <tr className="text-center">
          <td style={{width: "5%"}}>{product.id}</td>
          <td style={{width: "60%"}}>{product.title}</td>
          <td style={{width: "20%"}}>{util.formatCurrency(parseFloat(product.price))}</td>
          <td style={{width: "15%"}}>
            <Link to={`/admin/products/change/${product.id}`} className="btn btn-warning mr-1">
              <i className="fa fa-pencil" aria-hidden="true"></i>
            </Link>
            <button className="btn btn-danger" onClick={(e) => this.props.deleteProductAdmin(product)}>
              <i className="fa fa-times" aria-hidden="true"></i>
            </button>
          </td>
        </tr>
        </tbody>
      )
    )
    return (
      <div>
        <div>
          <h3>Товары</h3>
          <Link
            className="btn btn-success m-1 float-right"
            to="/admin/products/create"
          >
            Создать товар
          </Link>
        </div>
        <table className="table table-striped">
          <thead>
          <tr className="text-center">
            <th scope="col">ID</th>
            <th scope="col">Название</th>
            <th scope="col">Цена</th>
            <th scope="col">Управление</th>
          </tr>
          </thead>
          {productRecords}
        </table>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  products: state.adminProducts.products,
})

export default connect(mapStateToProps, {fetchProductsAdmin, deleteProductAdmin})(AdminProducts)