import React, {Component} from 'react';
import {connect} from "react-redux";
import {loginUser} from "../../actions/userAction";
import { Redirect } from "react-router-dom";
import {showAlert} from "../../actions/alertAction";
import util from "../../packs/util";

class LoginPage extends Component {
  state = {
    email: '',
    password: ''
  }

  handleSubmit = (e) => {
    e.preventDefault()

    if (this.state.email.length === 0){
      this.props.showAlert("Email не может быть пустым", "danger")
    } else if (this.state.password.length === 0) {
      this.props.showAlert("Пароль не может быть пустым", "danger")
    } else if (!util.validateEmail(this.state.email)) {
      this.props.showAlert("Формат email неправильный. Пример: example@email.com", "danger")
    } else if (this.state.password.length < 6) {
      this.props.showAlert("Пароль должен быть больше 6 символов", "danger")
    } else {
      this.props.loginUser(this.state)
    }

  }

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }

  render() {
    return (
      <div>
      { Object.keys(this.props.user).length === 0 ? (
        <div className="container">
          <h3>Вход</h3>
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label htmlFor="email">Email</label>
              <input
                onChange={this.handleChange}
                type="email"
                className="form-control"
                id="email"
                aria-describedby="emailHelp"
                placeholder="Введите email..."/>
            </div>
            <div className="form-group">
              <label htmlFor="password">Пароль</label>
              <input
                onChange={this.handleChange}
                type="password"
                className="form-control"
                id="password"
                placeholder="Введите пароль..."/>
            </div>
            <button type="submit" className="btn btn-primary">Войти</button>
          </form>
        </div>
        ) : (
          <Redirect to="/" />
        )
      }
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.currentUser,
})

export default connect(mapStateToProps, {showAlert, loginUser})(LoginPage)