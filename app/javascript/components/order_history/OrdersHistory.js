import React, {Component} from 'react';
import {connect} from "react-redux";
import {fetchOrdersHistory} from "../../actions/ordersHistoryAction";
import util from "../../packs/util";

class OrdersHistory extends Component {
  componentDidMount() {
    this.props.fetchOrdersHistory()
  }

  render() {
    const orderHistory = this.props.ordersHistoryItems.map(orderItem => (
        <tbody key={orderItem.id}>
          <tr className="text-center">
            <td style={{width: "15%"}}>{orderItem.order_id}</td>
            <td style={{width: "50%"}}>{orderItem.product.title}</td>
            <td style={{width: "15%"}}>{orderItem.quantity}</td>
            <td style={{width: "20%"}}>{util.formatCurrency(parseFloat(orderItem.sold_price))}</td>
          </tr>
        </tbody>
      )
    )
    return (
      <div>
        <div>
          <h3>История заказов</h3>
        </div>
        <table className="table table-striped">
          <thead>
          <tr className="text-center">
            <th scope="col">Номер заказа</th>
            <th scope="col">Товар</th>
            <th scope="col">Количество</th>
            <th scope="col">Итоговая стоимость</th>
          </tr>
          </thead>
          {orderHistory}
        </table>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  ordersHistoryItems: state.ordersHistory.items,
})

export default connect(mapStateToProps, {fetchOrdersHistory})(OrdersHistory)