import React, {Component} from 'react';
import {connect} from "react-redux";
import {hideAlert} from "../../actions/alertAction";

class Alert extends Component {

  render() {
    console.log(this.props.alert)
    if (!this.props.alert.visible) {
      return null
    }
    return (
      <div className={`alert alert-${this.props.alert.type || 'warning'} alert-dismissible`} role="alert">
        &nbsp;{this.props.alert.text}
        <button onClick={(e) => this.props.hideAlert()} type="button" className="close" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  alert: state.alert.alert,
})

export default connect(mapStateToProps, {hideAlert})(Alert)