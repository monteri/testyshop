import React, { Component } from 'react'
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import ProductsPage from "../components/products/ProductsPage";
import CartPage from "../components/cart/CartPage";
import LoginPage from "../components/user/LoginPage";
import RegistrationPage from "../components/user/RegistrationPage";
import {connect} from 'react-redux'
import Product from "../components/products/Product";
import PrivateRoute from "./PrivateRoute";
import AdminPage from "../components/admin/AdminPage";
import {fetchUser} from "../actions/userAction";
import Navbar from "../components/layout/Navbar";
import AdminRoute from "./AdminRoute";
import Alert from "../components/alert/Alert";
import OrdersHistory from "../components/order_history/OrdersHistory";

class App extends Component {
	componentDidMount() {
		console.log(localStorage.getItem("email"), localStorage.getItem("authentication_token"))
		if (localStorage.getItem("email") && localStorage.getItem("authentication_token")) {
			console.log("true")
			this.props.fetchUser()
		}
	}

	render() {
		console.log(this.props.user)
		return (
			<BrowserRouter>
				<Navbar />
				<div className="container pt-4">
					<Alert />
					<Switch>
						<Route exact path="/" component={ProductsPage} />
						<PrivateRoute exact path="/cart" component={CartPage} user={this.props.user}/>
						<PrivateRoute exact path="/history" component={OrdersHistory} user={this.props.user}/>
						<Route exact path="/login" component={LoginPage} />
						<Route exact path="/sign-up" component={RegistrationPage} />
						<Route exact path="/products/:id" component={Product} />
						<AdminRoute path="/admin" component={AdminPage} user={this.props.user} />
					</Switch>
				</div>
			</BrowserRouter>
		)
	}
}

const mapStateToProps = state => ({
	user: state.user.currentUser,
})

export default connect(mapStateToProps, {fetchUser})(App)