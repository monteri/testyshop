import React from 'react';
import { Route, Redirect } from 'react-router-dom';

function AdminRoute({ component: Component, user: user, ...rest }) {
  const isAuthenticated = Object.keys(user).length !== 0;
  const isAdmin = isAuthenticated && user.admin

  return(
    <Route {...rest}
           render={props =>
             isAdmin ? (
               <Component {...props} />
             ) : (
               <Redirect to="/" />
             )
           }
    />
  );
}

export default AdminRoute;