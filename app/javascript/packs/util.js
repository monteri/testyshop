
export default {
  formatCurrency: function (num) {
    return "₴" + Number(num.toFixed(2)).toLocaleString() + ' ';
  },
  validateEmail: function(email) {
    const re = /\S+@\S+\.\S+/
    return re.test(String(email).toLowerCase())
  }
}
