import {CHANGE_PRODUCT_ADMIN, CREATE_PRODUCT_ADMIN, DELETE_PRODUCT_ADMIN, FETCH_PRODUCTS_ADMIN} from "../actions/types";

const initialState = { products: [] }

export default function (state=initialState, action) {
  switch (action.type) {
    case FETCH_PRODUCTS_ADMIN:
      return { ...state, products: action.payload }
    case CREATE_PRODUCT_ADMIN:
      return { ...state, products: [ ...state.products, action.payload] }
    case CHANGE_PRODUCT_ADMIN:
      return { ...state, products: state.products.map(product =>
          product.id === action.payload.id ? { ...action.payload } : { ...product }) }
    case DELETE_PRODUCT_ADMIN:
      return { ...state, products: state.products.filter(product => product.id !== action.payload.id )}
    default:
      return state
  }
}