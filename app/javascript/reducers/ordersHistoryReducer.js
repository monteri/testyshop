import {FETCH_ORDERS_HISTORY} from "../actions/types";

const initialState = { items: []}

export default function (state=initialState, action) {
  switch (action.type) {
    case FETCH_ORDERS_HISTORY:
      return { ...state, items: action.payload }
    default:
      return state
  }
}