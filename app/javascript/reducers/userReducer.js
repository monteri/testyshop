import {FETCH_CURRENT_USER, LOGIN, LOGOUT, REGISTER} from "../actions/types";

const initialState = { currentUser: {} }

export default function (state=initialState, action) {
  switch (action.type) {
    case LOGIN:
      return { ...state, currentUser: action.payload }
    case REGISTER:
      return state
    case LOGOUT:
      return { ...state, currentUser: {} }
    case FETCH_CURRENT_USER:
      return { ...state, currentUser: action.payload }
    default:
      return state
  }
}