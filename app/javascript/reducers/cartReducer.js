import {ADD_TO_CART, CHANGE_IN_CART, CHECK_OUT_CART, FETCH_CART, REMOVE_FROM_CART} from "../actions/types";

const initialState = { items: [] }

export default function (state=initialState, action) {
  switch (action.type) {
    case ADD_TO_CART:
      return { ...state, items: state.items.map(item =>
          item.id === action.payload.id ? {...item, quantity: item.quantity++} : { ...item, quantity: 1}) }
    case FETCH_CART:
      return { ...state, items: action.payload }
    case REMOVE_FROM_CART:
      return { ...state, items: action.payload }
    case CHANGE_IN_CART:
      return { ...state, items: state.items.map(item =>
          item.id === action.payload.id ? { ...action.payload } : { ...item }) }
    case CHECK_OUT_CART:
      return { ...state, items: [] }
    default:
      return state
  }
}