import { combineReducers } from "redux";
import productsReducer from "./productsReducer";
import cartReducer from "./cartReducer";
import userReducer from "./userReducer";
import adminUsersReducer from "./adminUsersReducer";
import adminProductsReducer from "./adminProductsReducer";
import alertReducer from "./alertReducer";
import ordersHistoryReducer from "./ordersHistoryReducer";

export default combineReducers({
  products: productsReducer,
  cartItems: cartReducer,
  user: userReducer,
  adminUsers: adminUsersReducer,
  adminProducts: adminProductsReducer,
  alert: alertReducer,
  ordersHistory: ordersHistoryReducer
})