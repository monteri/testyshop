import {CHANGE_USER_ADMIN, CREATE_USER_ADMIN, DELETE_USER_ADMIN, FETCH_USERS_ADMIN} from "../actions/types";

const initialState = { users: []}

export default function (state=initialState, action) {
  switch (action.type) {
    case FETCH_USERS_ADMIN:
      return { ...state, users: action.payload }
    case CREATE_USER_ADMIN:
      return { ...state, users: [ ...state.users, action.payload] }
    case CHANGE_USER_ADMIN:
      return { ...state, users: state.users.map(user =>
          user.id === action.payload.id ? { ...action.payload } : { ...user }) }
    case DELETE_USER_ADMIN:
      return { ...state, users: state.users.filter(user => user.id !== action.payload.id )}
    default:
      return state
  }
}