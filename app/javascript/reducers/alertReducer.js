import {HIDE_ALERT, SHOW_ALERT} from "../actions/types";

const initialState = { alert: { visible: false }}

export default function (state=initialState, action) {
  switch (action.type) {
    case SHOW_ALERT:
      return { ...state, alert: { ...action.payload, visible: true } }
    case HIDE_ALERT:
      return { ...state, alert: { ...state.alert, visible: false } }
    default:
      return state
  }
}