import {FETCH_ORDERS_HISTORY} from "./types";

const HEADERS = () => {
  return {
    authentication_token: localStorage.getItem("authentication_token"),
    email: localStorage.getItem("email"),
    'Content-Type': 'application/json'
  }
}

const ORDER_ITEM_URL = "/api/v1/order_items"

export const fetchOrdersHistory = () => (dispatch, getState) => {
  const ORDER_ITEM_GET_PARAMS = {
    method: "GET",
    headers: HEADERS(),
  }
  const user = getState().user.currentUser
  fetch(ORDER_ITEM_URL + `?user=${user.id}&history=true`, ORDER_ITEM_GET_PARAMS)
    .then(res => res.json())
    .then(data => {
      console.log(data)
      return dispatch({
        type: FETCH_ORDERS_HISTORY,
        payload: data
      })
    })
}