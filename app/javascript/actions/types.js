export const FETCH_PRODUCTS = 'FETCH_PRODUCTS'
export const ADD_TO_CART = 'ADD_TO_CART'
export const FETCH_CART = 'FETCH_CART'
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART'
export const CHANGE_IN_CART = 'CHANGE_IN_CART'
export const CHECK_OUT_CART = 'CHECK_OUT_CART'
export const LOGIN = 'LOGIN'
export const LOGOUT = 'LOGOUT'
export const REGISTER = 'REGISTER'
export const FETCH_CURRENT_USER = 'FETCH_CURRENT_USER'
export const FETCH_USERS_ADMIN = 'FETCH_USERS_ADMIN'
export const CREATE_USER_ADMIN = 'CREATE_USER_ADMIN'
export const CHANGE_USER_ADMIN = 'CHANGE_USER_ADMIN'
export const DELETE_USER_ADMIN = 'DELETE_USER_ADMIN'
export const FETCH_PRODUCTS_ADMIN = 'FETCH_PRODUCTS_ADMIN'
export const CREATE_PRODUCT_ADMIN = 'CREATE_PRODUCT_ADMIN'
export const CHANGE_PRODUCT_ADMIN = 'CHANGE_PRODUCT_ADMIN'
export const DELETE_PRODUCT_ADMIN = 'DELETE_PRODUCT_ADMIN'
export const SHOW_ALERT = 'SHOW_ALERT'
export const HIDE_ALERT = 'HIDE_ALERT'
export const FETCH_ORDERS_HISTORY = 'FETCH_ORDERS_HISTORY'
