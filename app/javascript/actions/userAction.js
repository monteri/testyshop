import {FETCH_CURRENT_USER, LOGIN, LOGOUT, REGISTER, SHOW_ALERT} from "./types";

const LOGIN_URL = "/api/v1/sessions"
const REGISTRATION_URL = "/api/v1/registrations"
const USERS_URL = "/api/v1/users"
const HEADERS = () => {
  return {
    authentication_token: localStorage.getItem("authentication_token"),
    email: localStorage.getItem("email"),
    'Content-Type': 'application/json'
  }
}

export const loginUser = (user) => (dispatch) => {
  fetch(LOGIN_URL,
    {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      body: JSON.stringify({
        email: user.email,
        password: user.password
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      return dispatch(dispatch => {
        dispatch({
          type: LOGIN,
          payload: data.data.user
        })
        if (data.data.user !== undefined) {
          dispatch({
            type: SHOW_ALERT,
            payload: {
              text: "Вход выполнен успешно",
              type: "success"
            }
          })
          localStorage.setItem("authentication_token", data.data.user.authentication_token)
          localStorage.setItem("email", data.data.user.email)
          dispatch({
            type: FETCH_CURRENT_USER,
            payload: data.data.user
          })
          console.log(localStorage.getItem("authentication_token"))
          console.log(localStorage.getItem("email"))
        }
      })
    }).catch(error => {
      console.log(error)
      dispatch({
        type: SHOW_ALERT,
        payload: {
          text: "Email или пароль введен не верно",
          type: "danger"
        }
    })
  })
}

export const registerUser = (history, user) => (dispatch) => {
  fetch(REGISTRATION_URL,
    {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      body: JSON.stringify({
        email: user.email,
        password: user.password
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      return dispatch(dispatch => {
        dispatch({
          type: REGISTER
        })
        if (data.data.user !== undefined) {
          dispatch({
            type: SHOW_ALERT,
            payload: {
              text: "Регистрация успешна, подтвердите Ваш email адресс",
              type: "warning"
            }
          })
          history.push('/')
        } else {
          dispatch({
            type: SHOW_ALERT,
            payload: {
              text: "Пользователь с таким Email уже существует или пароль менее 6 символов",
              type: "danger"
            }
          })
        }
      })
    })
}

export const logoutUser = () => (dispatch) => {
  localStorage.removeItem("authentication_token")
  localStorage.removeItem("email")
  return dispatch(dispatch =>{
    dispatch({
      type: LOGOUT
    })
    dispatch({
      type: SHOW_ALERT,
      payload: {
        text: "Вы вышли из аккаунта",
        type: "warning"
      }
    })
  })
}

export const fetchUser = () => (dispatch) => {
  fetch(USERS_URL + "?by_email=" + localStorage.getItem("email"),
    {
      method: "GET",
      headers: HEADERS(),
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      return dispatch({
        type: FETCH_CURRENT_USER,
        payload: data
      })
    })
}

