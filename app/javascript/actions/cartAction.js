import {ADD_TO_CART, CHANGE_IN_CART, CHECK_OUT_CART, FETCH_CART, FETCH_CURRENT_USER, SHOW_ALERT} from "./types";

const HEADERS = () => {
  return {
    authentication_token: localStorage.getItem("authentication_token"),
    email: localStorage.getItem("email"),
    'Content-Type': 'application/json'
  }
}

const ORDER_ITEM_URL = "/api/v1/order_items"
const USERS_URL = "/api/v1/users"

const addProduct = (dispatch, user, product) => {
  const ORDER_ITEM_POST_PARAMS = {
    method: "POST",
    headers: HEADERS(),
    body: null
  }
  let body = {
    product_id: product.id,
    user_id: user.id,
    quantity: 1
  }
  if (user.current_order !== null) {
    body.order_id = user.current_order
  }
  fetch(ORDER_ITEM_URL,
    {...ORDER_ITEM_POST_PARAMS,
      body: JSON.stringify(body)
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      return dispatch(dispatch => {
        dispatch({
          type: ADD_TO_CART,
          payload: data
        })
        dispatch({
          type: SHOW_ALERT,
          payload: {
            text: `Товар \"${data.product.title}\" успешно добавлен в корзину`,
            type: "success"
          }
        })
        user.current_order = data.order_id
        dispatch({
          type: FETCH_CURRENT_USER,
          payload: user
        })
      })
    })
}

export const addToCart = (product) => (dispatch, getState) => {
  const user = getState().user.currentUser
  addProduct(dispatch, user, product)
}

export const fetchCart = () => (dispatch, getState) => {
  const ORDER_ITEM_GET_PARAMS = {
    method: "GET",
    headers: HEADERS(),
  }
  const user = getState().user.currentUser
  if (user.current_order !== null){
    fetch(ORDER_ITEM_URL + `?order=${user.current_order}`, ORDER_ITEM_GET_PARAMS)
      .then(res => res.json())
      .then(data => {
        if (data.length !== 0) {
          return dispatch({
            type: FETCH_CART,
            payload: data
          })
        }
      })
  }
}

export const deleteFromCart = (items, order_item) => (dispatch) => {
  const ORDER_ITEM_DELETE_PARAMS = {
    method: "DELETE",
    headers: HEADERS()
  }
  fetch(ORDER_ITEM_URL + `/${order_item.id}?product=${order_item.product.id}`,
    ORDER_ITEM_DELETE_PARAMS)
    .then(() => {
      const cartItems = items.filter(elm => elm.id !== order_item.id)
      console.log(cartItems)
      return dispatch(dispatch => {
        dispatch({
          type: FETCH_CART,
          payload: cartItems
        })
        dispatch({
          type: SHOW_ALERT,
          payload: {
            text: `Товар \"${order_item.product.title}\" удален из корзины`,
            type: "warning"
          }
        })
      })
    })
}

export const changeInCart = (items, order_item, quantity) => (dispatch) => {
  const ORDER_ITEM_PATCH_PARAMS = {
    method: "PATCH",
    headers: HEADERS()
  }
  fetch(ORDER_ITEM_URL + `/${order_item.id}`,
    {...ORDER_ITEM_PATCH_PARAMS,
      body: JSON.stringify({
        quantity: quantity
      })
    })
    .then(res => res.json())
    .then(data => {
      return dispatch(dispatch => {
        dispatch({
          type: CHANGE_IN_CART,
          payload: data
        })
        dispatch({
          type: SHOW_ALERT,
          payload: {
            text: `Количество изменено на ${data.quantity}`,
            type: "success"
          }
        })
      })
    })
}

export const checkOutCart = () => (dispatch, getState) => {
  const user = getState().user.currentUser
  fetch(USERS_URL + `/${user.id}`,
    {
      method: "PATCH",
      headers: HEADERS(),
      body: JSON.stringify({
        current_order: null
      })
    })
    .then(res => res.json())
    .then(data => {
      return dispatch(dispatch => {
        dispatch({
          type: CHECK_OUT_CART,
        })
        user.current_order = null
        dispatch({
          type: FETCH_CURRENT_USER,
          payload: user
        })
        dispatch({
          type: SHOW_ALERT,
          payload: {
            text: "Заказ оформлен успешно!",
            type: "success"
          }
        })
      })
    })
}