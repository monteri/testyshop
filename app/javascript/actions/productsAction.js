import {FETCH_PRODUCTS} from "./types";

export const fetchProducts = () => (dispatch) => {
  fetch("/api/v1/products",
    { headers: {
        authentication_token: localStorage.getItem("authentication_token"),
        email: localStorage.getItem("email")
      }})
    .then(res => res.json())
    .then(data => {
      console.log(data)
      return dispatch({
        type: FETCH_PRODUCTS,
        payload: data
      })
    })
}