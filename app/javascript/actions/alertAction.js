import {HIDE_ALERT, SHOW_ALERT} from "./types";

export const showAlert = (text, type='warning') => (dispatch) => {
  return dispatch({
    type: SHOW_ALERT,
    payload: {
      text: text,
      type: type
    }
  })
}

export const hideAlert = () => (dispatch) => {
  return dispatch({
    type: HIDE_ALERT
  })
}