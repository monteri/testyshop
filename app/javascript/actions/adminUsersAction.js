import {CHANGE_USER_ADMIN, CREATE_USER_ADMIN, DELETE_USER_ADMIN, FETCH_USERS_ADMIN, SHOW_ALERT} from "./types";

const USERS_URL = "/api/v1/users"
const HEADERS = () => {
  return {
    authentication_token: localStorage.getItem("authentication_token"),
    email: localStorage.getItem("email"),
    'Content-Type': 'application/json'
  }
}

export const fetchUsersAdmin = () => (dispatch) => {
  fetch(USERS_URL,
    { headers: HEADERS() })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      return dispatch({
        type: FETCH_USERS_ADMIN,
        payload: data
      })
    })
}

export const createUserAdmin = (user) => (dispatch) => {
  fetch(USERS_URL,
    {
      method: "POST",
      headers: HEADERS(),
      body: JSON.stringify({
        email: user.email,
        password: user.password,
        admin: user.admin
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      return dispatch(dispatch => {
        dispatch({
          type: CREATE_USER_ADMIN,
          payload: data
        })
        if (data.errors) {
          dispatch({
            type: SHOW_ALERT,
            payload: {
              text: "Такой email уже занят или пароль менее 6 символов",
              type: "danger"
            }
          })
        } else {
          dispatch({
            type: SHOW_ALERT,
            payload: {
              text: `Создан пользователь \"${data.email}\"`,
              type: "success"
            }
          })
        }
      })
    }).catch(error => {
      dispatch({
        type: SHOW_ALERT,
        payload: {
          text: "Не удалось создать пользователя",
          type: "danger"
        }
      })
    })
}

export const changeUserAdmin = (user) => (dispatch) => {
  fetch(USERS_URL + `/${user.id}`,
    {
      method: "PATCH",
      headers: HEADERS(),
      body: JSON.stringify({
        email: user.email,
        password: user.password,
        admin: user.admin
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      return dispatch(dispatch => {
        dispatch({
          type: CHANGE_USER_ADMIN,
          payload: data
        })
        if (data.errors) {
          dispatch({
            type: SHOW_ALERT,
            payload: {
              text: "Такой email уже занят или пароль менее 6 символов",
              type: "danger"
            }
          })
        } else {
          dispatch({
            type: SHOW_ALERT,
            payload: {
              text: `Изменен пользователь \"${data.email}\"`,
              type: "success"
            }
          })
        }
      })
    }).catch(error => {
      dispatch({
        type: SHOW_ALERT,
        payload: {
          text: "Не удалось изменить пользователя",
          type: "danger"
        }
      })
  })
}

export const deleteUserAdmin = (user) => (dispatch) => {
  fetch(USERS_URL + `/${user.id}`,
    {
      method: "DELETE",
      headers: HEADERS(),
    })
    .then(() => {
      return dispatch(dispatch => {
        dispatch({
          type: DELETE_USER_ADMIN,
          payload: user
        })
        dispatch({
          type: SHOW_ALERT,
          payload: {
            text: `Пользователь ${user.email} удален`,
            type: "warning"
          }
        })
      })
    })
}

