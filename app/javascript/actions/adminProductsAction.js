import {
  CHANGE_PRODUCT_ADMIN, CREATE_PRODUCT_ADMIN, DELETE_PRODUCT_ADMIN, FETCH_PRODUCTS_ADMIN, SHOW_ALERT
} from "./types";

const PRODUCTS_URL = "/api/v1/products"
const HEADERS = () => {
  return {
    authentication_token: localStorage.getItem("authentication_token"),
    email: localStorage.getItem("email"),
    // 'Content-Type': 'application/json'
  }
}

export const fetchProductsAdmin = () => (dispatch) => {
  fetch(PRODUCTS_URL,
    { headers: HEADERS() })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      return dispatch({
        type: FETCH_PRODUCTS_ADMIN,
        payload: data
      })
    })
}

export const createProductAdmin = (product) => (dispatch) => {
  const formData = new FormData()
  formData.append('title', product.title)
  formData.append('price', product.price)
  formData.append('category_id', 1)
  product.image !== null && formData.append('image', product.image)
  fetch(PRODUCTS_URL,
    {
      method: "POST",
      headers: HEADERS(),
      body: formData
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      return dispatch(dispatch => {
        dispatch({
          type: CREATE_PRODUCT_ADMIN,
          payload: data
        })
        if (data.errors) {
          dispatch({
            type: SHOW_ALERT,
            payload: {
              text: "Не удалось создать товар",
              type: "danger"
            }
          })
        } else {
          dispatch({
            type: SHOW_ALERT,
            payload: {
              text: `Создан товар \"${data.title}\"`,
              type: "success"
            }
          })
        }
      })
    }).catch(error => {
    dispatch({
      type: SHOW_ALERT,
      payload: {
        text: "Не удалось создать товар",
        type: "danger"
      }
    })
  })
}

export const changeProductAdmin = (product) => (dispatch) => {
  const formData = new FormData()
  formData.append('title', product.title)
  formData.append('price', product.price)
  formData.append('category_id', 1)
  product.image !== null && formData.append('image', product.image)
  fetch(PRODUCTS_URL + `/${product.id}`,
    {
      method: "PATCH",
      headers: HEADERS(),
      body: formData
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      return dispatch(dispatch => {
        dispatch({
          type: CHANGE_PRODUCT_ADMIN,
          payload: data
        })
        if (data.errors) {
          dispatch({
            type: SHOW_ALERT,
            payload: {
              text: "Не удалось изменить товар",
              type: "danger"
            }
          })
        } else {
          dispatch({
            type: SHOW_ALERT,
            payload: {
              text: `Изменен товар \"${data.title}\"`,
              type: "success"
            }
          })
        }
      })
    }).catch(error => {
      dispatch({
        type: SHOW_ALERT,
        payload: {
          text: "Не удалось изменить товар",
          type: "danger"
        }
      })
  })
}

export const deleteProductAdmin = (product) => (dispatch) => {
  fetch(PRODUCTS_URL + `/${product.id}`,
    {
      method: "DELETE",
      headers: HEADERS(),
    })
    .then(() => {
      return dispatch(dispatch => {
        dispatch({
          type: DELETE_PRODUCT_ADMIN,
          payload: product
        })
        dispatch({
          type: SHOW_ALERT,
          payload: {
            text: `Товар "${product.title}" удален`,
            type: "warning"
          }
        })
      })
    })
}