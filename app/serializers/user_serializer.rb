class UserSerializer < ApplicationSerializer
  attributes :id, :email, :admin, :current_order
end
