class CategorySerializer < ApplicationSerializer
  attributes :id, :title
  has_many :products, serializer: ProductSerializer
end
