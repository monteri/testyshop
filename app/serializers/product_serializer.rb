class ProductSerializer < ApplicationSerializer
  attributes :id, :title, :price, :category_id
  attributes :image

  def image
    rails_blob_path(object.image, only_path: true) if object.image.attached?
  end
end
