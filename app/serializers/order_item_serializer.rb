class OrderItemSerializer < ApplicationSerializer
  attributes :id, :order_id, :quantity, :sold_price
  belongs_to :user
  belongs_to :product
end
