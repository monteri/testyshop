class ApplicationMailer < ActionMailer::Base
  default from: 'monteribot@gmail.com'
  layout 'mailer'
end
