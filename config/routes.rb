Rails.application.routes.draw do
  devise_for :users, only: [:confirmations], controllers: { confirmations: 'confirmations' }
  get 'welcome', to: 'welcome#index', as: 'welcome'

  namespace :api, defaults: {format: :json} do
    namespace :v1 do
      resources :registrations, only: [:create]
      resources :sessions, only: [:create]
      resources :products
      resources :categories
      resources :users
      resources :order_items
      resources :orders
    end
  end
  root 'home#index'
  get '*path', to: 'home#index', constraints: lambda { |req|
    req.path.exclude? 'rails/active_storage'
  }
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
